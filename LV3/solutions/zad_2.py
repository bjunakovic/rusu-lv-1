# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 13:57:47 2021

@author: student
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

def getCyl4(mtcars):
    cyl4 = mtcars[mtcars.cyl == 4]
    return cyl4

def getCyl6(mtcars):
    cyl6 = mtcars[mtcars.cyl == 6]
    return cyl6

def getCyl8(mtcars):
    cyl8 = mtcars[mtcars.cyl == 8]
    return cyl8

cyl4 = getCyl4(mtcars)
cyl6 = getCyl6(mtcars)
cyl8 = getCyl8(mtcars)

plt.figure()
plt.bar(["cyl4", "cyl6", "cyl8"],[cyl4["mpg"].mean(), cyl6["mpg"].mean(), cyl8["mpg"].mean()])

data = [[cyl4["mpg"]],[cyl6["mpg"]],[cyl8["mpg"]]]
DFData = pd.DataFrame(list(zip(data[0],data[1],data[2])),columns = ["cyl4", "cyl6", "cyl8"])

print(data[0].tolist())

plt.figure()
DFData.boxplot()