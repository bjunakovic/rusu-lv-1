# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 12:20:36 2021

@author: student
"""

import pandas as pd
import numpy as np


mtcars = pd.read_csv('mtcars.csv')

def getTop38cilLowestMpg(mtcars):
    top3_8cilLowestMpg = mtcars.sort_values(by="mpg")
    top3_8cilLowestMpg = (top3_8cilLowestMpg[top3_8cilLowestMpg.cyl == 8])[0:3]
    return top3_8cilLowestMpg

def getCyl6mean(mtcars):
    cyl6 = mtcars[mtcars.cyl ==6]
    return cyl6.mean().mpg

def getCyl4MpgMean(mtcars):
    cyl4 = mtcars[mtcars.cyl ==4]
    cyl4 = cyl4[(cyl4.wt>=2.0) & (cyl4.wt<=2.2)]
    return cyl4.mean().mpg

def getAutoCount(mtcars):
    automatic = mtcars[mtcars.am == 0]
    return len(automatic)

def getManualCount(mtcras):
    return (len(mtcars) - getAutoCount(mtcars))

def getAuto100HPCount(mtcars):
    automatic = mtcars[mtcars.am == 0]
    automatic = automatic[automatic.hp > 120]
    return len(automatic)

def showKg(mtcars):
    mtcarsKg = mtcars
    mtcarsKg["wt"] = 453.59237 * mtcarsKg["wt"]
    print (mtcarsKg[["car","wt"]])
        

top5greatestMpg = mtcars.sort_values(by="mpg", ascending = False)[0:10]
top3_8cilLowestMpg = getTop38cilLowestMpg(mtcars)
cyl6meanMpg = getCyl6mean(mtcars)
cyl4meanMpg = getCyl4MpgMean(mtcars)
automaticCount = getAutoCount(mtcars)
manualCount = getManualCount(mtcars)
auto100hpcount = getAuto100HPCount(mtcars)


print(top5greatestMpg)
print(top3_8cilLowestMpg)
print(cyl6meanMpg)
print(cyl4meanMpg)
print(automaticCount)
print(manualCount)
print(auto100hpcount)
showKg(mtcars)

print(auto100hpcount)