import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf 
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from shutil import copy2
import os

testData = pd.read_csv('Test.csv', index_col = 0)
print(testData)
path = os.getcwd()
os.makedirs('Test_Dir', exist_ok=True)
for i in range (43):
    os.makedirs(path + '/Test_Dir/' + str(i), exist_ok=True)
    
    
for _, row in testData.iterrows():
    endpath = row.Path.split('/')
    name = endpath[1]
    copy2(row.Path, path + '/Test_Dir/' + str(row.ClassId) + '/' + name)
    
datasetTest = keras.utils.image_dataset_from_directory('Test_Dir')
datasetTrain = keras.utils.image_dataset_from_directory('Train')


num_classes = 43
input_shape = (48, 48, 3)

model = keras.Sequential()
model.add(keras.Input(input_shape))
model.add(layers.Conv2D(32, kernel_size=(3, 3), activation='relu'))
model.add(layers.Conv2D(32, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.2))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.2))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.2))
model.add(layers.Flatten())
model.add(layers.Dense(512, activation = 'relu', ))
model.add(layers.Dropout(0.5))
model.add(layers.Dense(num_classes,activation = 'softmax'))
model.summary()

