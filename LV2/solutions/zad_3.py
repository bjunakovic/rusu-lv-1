# -*- coding: utf-8 -*-
"""
Created on Fri Nov 12 18:56:02 2021

@author: student
"""

import numpy as np
import random
import matplotlib.pyplot as plt

people = np.zeros(10)
height = np.zeros(10)

for n in range(10):
    people[n] = random.randint(0, 1)
    if(n==1):
        height[n] = np.random.normal(180,7,1)
    else:
        height[n] = np.random.normal(167,7,1)

plt.hist(people,height)
plt.xlabel('gender')
plt.ylabel('height')