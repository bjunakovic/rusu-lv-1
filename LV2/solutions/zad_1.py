# -*- coding: utf-8 -*-
"""
Created on Fri Nov 12 17:22:57 2021

@author: student
"""

import re

#fname = input('Enter the file name: ')
fname = 'mbox-short.txt'
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()
    
hosts = []
emails = dict()

for line in list(fhand.readlines()):
    line = line.rstrip()
    adrs = re.findall('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+', line)
    for adr in adrs:
        host = re.findall('\S*@', adr)
        hosts.append(host)

print (hosts)
