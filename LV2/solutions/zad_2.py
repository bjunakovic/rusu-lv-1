# -*- coding: utf-8 -*-
"""
Created on Fri Nov 12 18:07:21 2021

@author: student
"""

import re

#fname = input('Enter the file name: ')
fname = 'mbox-short.txt'
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()
    
hosts = []
emails = dict()

for line in list(fhand.readlines()):
    line = line.rstrip()
    adrs = re.findall('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+', line)
    for adr in adrs:
        host = re.findall('\S*@', adr)
        hosts.append(host)

ALonea = []
onea = []
noa = []
num = []
lower = []

for name in hosts:
    if (re.search('a+', str(name))):
        ALonea.append(name)
    if (re.search('a', str(name))):
        onea.append(name)
    if (re.fullmatch('[^a]*', str(name))):
        noa.append(name)
    if (re.search('[0-9]+', str(name))):
        num.append(name)
    if (re.fullmatch('[a-z]*@', str(name[0]))):
        lower.append(name)

    
print (hosts[-10:])
print (ALonea[-10:])
print (onea[-10:])
print (noa[-10:])
print (num[-10:])
print (lower[-10:])
