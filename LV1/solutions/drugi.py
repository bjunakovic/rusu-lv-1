# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 14:20:10 2021

@author: student
"""

grade = -1.0

while grade > 1.0 or grade < 0.0:
    grade = float(input('Enter grade [0.0,1.0]: '))
    
if grade < 0.6:
    print('F')
elif grade < 0.7:
    print('D')
elif grade < 0.8:
    print('C')
elif grade < 0.9:
    print('B')
else:
    print('A')
