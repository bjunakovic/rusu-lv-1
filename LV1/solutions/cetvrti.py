# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 14:33:13 2021

@author: student
"""

import math

numbers = []
endString = 'Done'

while(True):
    try:
        userInput = input('Number: ')
        if userInput == endString:
            break
        else:
            userInput = int(userInput)
            numbers.append(int((userInput)))
    except ValueError:
        print("Not int or keyword Done")
        
sum = sum(numbers)
if sum != 0:
    avg = sum/len(numbers)
    print('size = ' + str(len(numbers)))
    print('average = ' + str(avg))
    print('min = ' + str(min(numbers)))
    print('max = ' + str(max(numbers)))
else:
    print('Zero sum')