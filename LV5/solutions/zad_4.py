from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


image = mpimg.imread('example_grayscale.png')

uniqueValues = np.unique(image)
print(len(uniqueValues))

imageReshaped = image.reshape((-1, 1))
k_means = cluster.KMeans(n_clusters=10,n_init=1)
k_means.fit(imageReshaped)
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
imageCompressed = np.choose(labels, values)
imageCompressed.shape = image.shape

plt.figure(1)
plt.imshow(image,  cmap='gray')

plt.figure(2)
plt.imshow(imageCompressed,  cmap='gray')