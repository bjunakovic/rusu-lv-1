# -*- coding: utf-8 -*-
"""
Created on Tue Dec 14 15:44:47 2021

@author: student
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as cluster
import sklearn.datasets as datasets
import random


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          centers = 4,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

dataset = generate_data(500, 1)

plt.scatter(
   dataset[:, 0], dataset[:, 1],
   c='white', marker='o',
   edgecolor='black', s=50
)
plt.show()

numberOfClusters = int(input("Number of clusters:"))

km = cluster.KMeans(
    n_clusters=numberOfClusters, init='random',
    n_init=10, max_iter=300, 
    tol=1e-04, random_state=0
)
y_km = km.fit_predict(dataset)

for clstr in range (numberOfClusters):
    r = random.random()
    b = random.random()
    g = random.random()
    color = (r, g, b)
    plt.scatter(
    dataset[y_km == clstr, 0], dataset[y_km == clstr, 1],
    s=50, color=color,
    marker='o', edgecolor='black',
    label='cluster' + str(clstr)
)


# plot the centroids
plt.scatter(
    km.cluster_centers_[:, 0], km.cluster_centers_[:, 1],
    s=250, marker='*',
    c='red', edgecolor='black',
    label='centroids'
)
plt.legend(scatterpoints=1)
plt.grid()
plt.show()





